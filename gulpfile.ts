import { watch, series, src, dest } from "gulp";
import { createProject } from "gulp-typescript";
import { init, write } from "gulp-sourcemaps";
// import istanbul from "gulp-istanbul";
import mocha from "gulp-mocha";
import del from "del";

const tsProject = createProject("tsconfig.json");

function clean() {
    return del("./dist");
}

function translate() {
    return tsProject
        .src()
        .pipe(init())
        .pipe(tsProject())
        .pipe(write("maps/"))
        .pipe(dest("dist/"));
}

function test() {
    return src("dist/test/**/*.js").pipe(mocha());
}

function watchTranslateAndTest() {
    return watch(
        ["./src/**/*.ts", "package.json"],
        {},
        series(clean, translate, test)
    );
}

function watchTranslate() {
    return watch(
        ["./src/**/*.ts", "package.json"],
        {},
        series(clean, translate)
    );
}

function watchTest() {
    return watch(["./dist/**/*.js", "package.json"], {}, test);
}

exports.default = watchTranslateAndTest;
exports.all = series(clean, translate, test);

exports.clean = clean;
exports.translate = series(clean, translate);
exports.test = test;

exports.watchTest = watchTest;
exports.watchTranslate = watchTranslate;
