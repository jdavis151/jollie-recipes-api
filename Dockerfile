# Define the base image as long term support version of node.
FROM node:lts

# Labels
LABEL author="John Davis <jcdavis151@gmail.com>"

WORKDIR /usr/src/app

EXPOSE 3000

# Get our npm dependecy list so we can install.
COPY package.json .

# Install npm dependencies.
RUN npm install -g gulp-cli --loglevel=error && \
    npm install --loglevel=error

# Copy the rest of the app.
COPY . .

ENTRYPOINT ["npm", "run", "full-serve"]
