import express from "express";
import {
    userRoute,
    defaultRoute,
    categoryRoute,
    ingredientRoute,
    recipeRoute,
} from "../controllers";

const router = express.Router();

router.get("/", defaultRoute);
router.get("/user/", userRoute);
router.get("/category/", categoryRoute);
router.get("/ingredient/", ingredientRoute);
router.get("/recipe/", recipeRoute);

export default router;
