import { Pool, PoolClient } from "pg";
import { pool } from "./pool";

class Model {
    pool: Pool;
    table: any;

    constructor(table: any) {
        this.pool = pool;
        this.table = table;
        this.pool.on(
            "error",
            (err: Error, client: PoolClient) =>
                `Error, ${err}, on idle client${client}`
        );
    }
}
