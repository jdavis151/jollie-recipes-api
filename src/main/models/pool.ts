import { Pool } from "pg";
import { DB_STRING } from "../../resources/constants";

export const pool: Pool = new Pool({ connectionString: DB_STRING });
