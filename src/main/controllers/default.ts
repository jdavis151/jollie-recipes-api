import { Request, Response } from "express";

export const defaultRoute = (req: Request, res: Response, next: any) =>
    res.status(200).json({ message: "Default Route" });
