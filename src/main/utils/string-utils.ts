/**
 * Utilities for strings
 *
 * @author jcdavis151
 */

/**
 * Trims execessive whitespace from a string.
 * @param str
 */
export function trim(str: string): string {
    return str.replace(/\s+/g, " ").trim();
}
