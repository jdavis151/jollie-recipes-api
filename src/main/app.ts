import express from "express";
import indexRouter from "./routes/index";
import { DEFAULT_PORT } from "../resources/constants";

const port = DEFAULT_PORT || 3000;
const app = express();
app.use(express.json());
app.use("/jollie", indexRouter);

app.set("port", port);
app.listen(port);

export default app;
