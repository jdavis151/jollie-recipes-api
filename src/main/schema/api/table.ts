import { trim } from "../../utils/string-utils";
import Column from "./column";

/**
 * Abstract Class describing standard functionality of all Tables
 *
 * @author jcdavis151
 */
export default abstract class Table {
    protected static tableName: string;
    protected static idColumn: Column;
    protected static columnMap: Column[];

    constructor() {}

    static getTableName(): string {
        return this.tableName;
    }

    static getIdColumn(): Column {
        return this.idColumn;
    }

    static getColumnMap(): Column[] {
        return this.columnMap;
    }

    static getCleanCreationString(): string {
        return trim(`${this.getDropString()}
        ${this.getCreationString()}`);
    }

    static getCreationString(): string {
        let columnStrings: string = this.columnMap.map((col: Column) =>
            col.getCreationString()
        ).join(`,
            `);

        return trim(`CREATE TABLE IF NOT EXISTS ${this.tableName} (
            ${columnStrings}
        );`);
    }

    static getDropString(): string {
        return trim(`DROP TABLE IF EXISTS ${this.tableName};`);
    }
}
