import { trim } from "../../utils/string-utils";
import {
    inlineCheck,
    inlineNotNull,
    inlinePrimaryKey,
    inlineUnique,
} from "../utils/constraints";
import { DataType } from "../utils/data-types";

/**
 *
 *
 * @author jcdavis151
 */
export default class Column {
    readonly name: string;
    readonly type: DataType;
    readonly primaryKey?: boolean;
    readonly notNull?: boolean;
    readonly unique?: boolean;
    readonly check?: string;

    constructor(
        name: string,
        type: DataType,
        primaryKey?: boolean,
        notNull?: boolean,
        unique?: boolean,
        check?: string
    ) {
        this.name = name;
        this.type = type;
        this.primaryKey = primaryKey || false;
        this.notNull = notNull || false;
        this.unique = unique || false;
        this.check = check || "";
    }

    getName(): string {
        return this.name;
    }

    getType(): DataType {
        return this.type;
    }

    getCreationString(): string {
        let str = ` ${this.name} ${this.type} `;

        if (this.notNull) {
            str += ` ${inlineNotNull()} `;
        }

        if (this.unique) {
            str += ` ${inlineUnique()} `;
        }

        if (this.primaryKey) {
            str += ` ${inlinePrimaryKey()} `;
        }

        if (this.check) {
            str += ` ${inlineCheck(this.check)} `;
        }

        return trim(str);
    }

    getAdditionString(): string {
        return trim(` ADD COLUMN ${this.getCreationString()} `);
    }
}
