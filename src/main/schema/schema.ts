import Table from "./api/table";
import AppUserTable from "./tables/app-user";
import CategoryTable from "./tables/category";
import IngredientTable from "./tables/ingredient";
import InstructionTable from "./tables/instruction";
import RecipeTable from "./tables/recipe";
import RecipeIngredientTable from "./tables/recipe-ingredient";
/**
 * The schema for Jollie Recipes.
 *
 * @author jcdavis151
 */
export default class Schema {
    /**
     * Name of the schema.
     */
    static schemaName: string = "jollieRecipes";

    /**
     * Ordered list of tables in the schema.
     */
    static schemaTables: any[] = [
        AppUserTable,
        CategoryTable,
        IngredientTable,
        RecipeTable,
        RecipeIngredientTable,
        InstructionTable,
    ];

    /**
     * Creates an SQL string from creating the entire schema.
     *
     * @return {string}
     */
    static getCleanCreationString(): string {
        let str: string = "";

        this.schemaTables.forEach(
            (table) => (str += table.getCleanCreationString())
        );

        return str.trim();
    }
}
