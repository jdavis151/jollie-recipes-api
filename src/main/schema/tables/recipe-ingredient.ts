import Table from "../api/table";
import Column from "../api/column";
import { DataType } from "../utils/data-types";
/**
 * The Recipe-Ingredient table for the schema
 *
 * @author jcdavis151
 */
export default class RecipeIngredientTable extends Table {
    static tableName: string = "recipeIngredient";

    static idCol: Column = new Column("id", DataType.Serial, true);
    static ingredientCol: Column = new Column(
        "ingredient",
        DataType.Serial,
        false,
        true
    );
    static recipeCol: Column = new Column(
        "recipe",
        DataType.Serial,
        false,
        true
    );
    static quantityCol: Column = new Column(
        "quantity",
        DataType.String,
        false,
        true
    );
    static rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    static idColumn: Column = RecipeIngredientTable.idCol;

    static columnMap: Column[] = [
        RecipeIngredientTable.idCol,
        RecipeIngredientTable.ingredientCol,
        RecipeIngredientTable.recipeCol,
        RecipeIngredientTable.quantityCol,
        RecipeIngredientTable.rectimestampCol,
    ];
}
