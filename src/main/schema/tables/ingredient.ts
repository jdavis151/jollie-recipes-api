import Table from "../api/table";
import Column from "../api/column";
import { DataType } from "../utils/data-types";
/**
 * The Ingredient table for the schema
 *
 * @author jcdavis151
 */
export default class IngredientTable extends Table {
    static tableName: string = "ingredient";

    static idCol: Column = new Column("id", DataType.Serial, true);
    static nameCol: Column = new Column(
        "name",
        DataType.String,
        false,
        true,
        true
    );
    static descCol: Column = new Column(
        "description",
        DataType.String,
        false,
        true
    );
    static rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    static idColumn: Column = IngredientTable.idCol;

    static columnMap: Column[] = [
        IngredientTable.idCol,
        IngredientTable.nameCol,
        IngredientTable.descCol,
        IngredientTable.rectimestampCol,
    ];
}
