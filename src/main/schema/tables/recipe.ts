import Table from "../api/table";
import Column from "../api/column";
import { DataType } from "../utils/data-types";
/**
 * The Recipe table for the schema
 *
 * @author jcdavis151
 */
export default class RecipeTable extends Table {
    static tableName: string = "recipe";

    static idCol: Column = new Column("id", DataType.Serial, true);
    static nameCol: Column = new Column(
        "name",
        DataType.String,
        false,
        true,
        true
    );
    static descCol: Column = new Column("description", DataType.String);
    static rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    static idColumn: Column = RecipeTable.idCol;

    static columnMap: Column[] = [
        RecipeTable.idCol,
        RecipeTable.nameCol,
        RecipeTable.descCol,
        RecipeTable.rectimestampCol,
    ];
}
