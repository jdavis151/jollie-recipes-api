import Table from "../api/table";
import Column from "../api/column";
import { DataType } from "../utils/data-types";
/**
 * The Instruction table for the schema
 *
 * @author jcdavis151
 */
export default class InstructionTable extends Table {
    static tableName: string = "instruction";

    static idCol: Column = new Column("id", DataType.Serial, true);
    static titleCol: Column = new Column("title", DataType.String, false, true);
    static descCol: Column = new Column(
        "description",
        DataType.String,
        false,
        true
    );
    static recipeCol: Column = new Column(
        "recipeId",
        DataType.Serial,
        false,
        true
    );
    static orderCol: Column = new Column("order", DataType.Integer);
    static rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    static idColumn: Column = InstructionTable.idCol;

    static columnMap: Column[] = [
        InstructionTable.idCol,
        InstructionTable.titleCol,
        InstructionTable.descCol,
        InstructionTable.recipeCol,
        InstructionTable.orderCol,
        InstructionTable.rectimestampCol,
    ];
}
