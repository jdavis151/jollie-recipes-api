import Table from "../api/table";
import Column from "../api/column";
import { DataType } from "../utils/data-types";
/**
 * The AppUser table for the schema
 *
 * @author jcdavis151
 */
export default class AppUserTable extends Table {
    static tableName: string = "appUser";

    static idCol: Column = new Column("id", DataType.Serial, true);
    static nameCol: Column = new Column("name", DataType.String, false, true);
    static rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    static idColumn: Column = AppUserTable.idCol;

    static columnMap: Column[] = [
        AppUserTable.idCol,
        AppUserTable.nameCol,
        AppUserTable.rectimestampCol,
    ];
}
