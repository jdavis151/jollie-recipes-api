import { trim } from "../../utils/string-utils";

/**
 * Functions for defining constraints in SQL.
 */
export enum ForeignTrigger {
    Update = "ON UPDATE",
    Delete = "ON DELETE",
}

export enum ForeignAction {
    SetNull = "SET NULL",
    Cascade = "CASCADE",
}

export function inlinePrimaryKey(): string {
    return trim(` PRIMARY KEY `);
}

export function inlineForeignKey(
    refTableName: string,
    refColumnName: string
): string {
    return trim(` FOREIGN KEY REFERENCES ${refTableName}(${refColumnName}) `);
}

export function inlineCheck(expression: string): string {
    return trim(` CHECK (${expression}) `);
}

export function inlineNotNull(): string {
    return trim(` NOT NULL `);
}

export function inlineUnique(): string {
    return trim(` UNIQUE `);
}

export function basicPrimaryKey(columnName: string): string {
    return trim(` PRIMARY KEY(${columnName}) `);
}

export function basicForeignKey(
    columnName: string,
    refTableName: string,
    refColumnName: string,
    refTrigger?: ForeignTrigger,
    refAction?: ForeignAction
): string {
    let baseStr = ` FOREIGN KEY(${columnName}) REFERENCES ${refTableName}(${refColumnName}) `;

    if (refTrigger && refAction) {
        baseStr += ` ${refTrigger} ${refAction} `;
    }

    return trim(baseStr);
}
