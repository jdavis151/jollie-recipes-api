/**
 * Enumeration of PostgreSQL data types.
 *
 * @author jcdavis151
 */
export enum DataType {
    Integer = "INTEGER",
    Float = "REAL",
    Timestamp = "TIMESTAMP",
    Boolean = "BOOLEAN",
    String = "VARCHAR",
    Serial = "SERIAL",
}
