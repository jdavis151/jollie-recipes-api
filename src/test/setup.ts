import supertest from "supertest";
import app from "../main/app";

const server = supertest.agent(app);
const BASE_URL = "/jollie";

export { server, BASE_URL };
