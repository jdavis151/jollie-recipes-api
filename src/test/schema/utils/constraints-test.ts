import { expect } from "chai";
import {
    basicForeignKey,
    basicPrimaryKey,
    ForeignAction,
    ForeignTrigger,
    inlineForeignKey,
    inlineNotNull,
    inlinePrimaryKey,
    inlineUnique,
} from "../../../main/schema/utils/constraints";

describe("Constraints", function () {
    it("inlinePrimaryKey", function () {
        // Arrange/Act/Assert
        expect(inlinePrimaryKey()).to.equal("PRIMARY KEY");
    });

    it("inlineForeignKey", function () {
        // Arrange
        let table = "table";
        let column = "column";

        // Act/Assert
        expect(inlineForeignKey(table, column)).to.equal(
            "FOREIGN KEY REFERENCES table(column)"
        );
    });

    it("inlineNotNull", function () {
        // Arrange/Act/Assert
        expect(inlineNotNull()).to.equal("NOT NULL");
    });

    it("inlineUnique", function () {
        // Arrange/Act/Assert
        expect(inlineUnique()).to.equal("UNIQUE");
    });

    it("basicPrimaryKey", function () {
        // Arrange
        let column = "column";

        // Act/Assert
        expect(basicPrimaryKey(column)).to.equal("PRIMARY KEY(column)");
    });

    it("basicForeignKey", function () {
        // Arrange
        let column = "column";
        let refTable = "refTable";
        let refColumn = "refColumn";
        let refTrigger = ForeignTrigger.Delete;
        let refAction = ForeignAction.Cascade;

        // Act
        let def = basicForeignKey(column, refTable, refColumn);
        let full = basicForeignKey(
            column,
            refTable,
            refColumn,
            refTrigger,
            refAction
        );

        // Assert
        expect(def).to.equal(
            "FOREIGN KEY(column) REFERENCES refTable(refColumn)"
        );
        expect(full).to.equal(
            "FOREIGN KEY(column) REFERENCES refTable(refColumn) ON DELETE CASCADE"
        );
    });
});
