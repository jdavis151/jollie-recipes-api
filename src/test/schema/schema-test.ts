import { expect } from "chai";
import Schema from "../../main/schema/schema";
import AppUserTable from "../../main/schema/tables/app-user";
import CategoryTable from "../../main/schema/tables/category";
import IngredientTable from "../../main/schema/tables/ingredient";
import InstructionTable from "../../main/schema/tables/instruction";
import RecipeTable from "../../main/schema/tables/recipe";
import RecipeIngredientTable from "../../main/schema/tables/recipe-ingredient";

describe("Schema class", function () {
    it("static properties", function () {
        // Arrange/Act/Assert
        expect(Schema.schemaName).to.equal("jollieRecipes");
        expect(Schema.schemaTables).to.deep.equal([
            AppUserTable,
            CategoryTable,
            IngredientTable,
            RecipeTable,
            RecipeIngredientTable,
            InstructionTable,
        ]);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS appUser; CREATE TABLE IF NOT EXISTS appUser ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );" +
            "DROP TABLE IF EXISTS category; CREATE TABLE IF NOT EXISTS category ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );" +
            "DROP TABLE IF EXISTS ingredient; CREATE TABLE IF NOT EXISTS ingredient ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );" +
            "DROP TABLE IF EXISTS recipe; CREATE TABLE IF NOT EXISTS recipe ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR, rectimestamp TIMESTAMP NOT NULL );" +
            "DROP TABLE IF EXISTS recipeIngredient; CREATE TABLE IF NOT EXISTS recipeIngredient ( id SERIAL PRIMARY KEY, ingredient SERIAL NOT NULL, recipe SERIAL NOT NULL, quantity VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );" +
            "DROP TABLE IF EXISTS instruction; CREATE TABLE IF NOT EXISTS instruction ( id SERIAL PRIMARY KEY, title VARCHAR NOT NULL, description VARCHAR NOT NULL, recipeId SERIAL NOT NULL, order INTEGER, rectimestamp TIMESTAMP NOT NULL );".trim();

        // Act
        let actualStr = Schema.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
