import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import CategoryTable from "../../../main/schema/tables/category";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Category Table class", function () {
    let idCol: Column = new Column("id", DataType.Serial, true);
    let nameCol: Column = new Column(
        "name",
        DataType.String,
        false,
        true,
        true
    );
    let descCol: Column = new Column(
        "description",
        DataType.String,
        false,
        true
    );
    let rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    it("static properties", function () {
        // Arrange/Act/Assert
        expect(CategoryTable.getTableName()).to.equal("category");
        expect(CategoryTable.getIdColumn()).to.deep.equal(idCol);
        expect(CategoryTable.getColumnMap()).to.deep.equal([
            idCol,
            nameCol,
            descCol,
            rectimestampCol,
        ]);
    });

    it("getDropString", function () {
        // Arrange
        let expectedStr = "DROP TABLE IF EXISTS category;";

        // Act
        let actualStr = CategoryTable.getDropString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCreationString", function () {
        // Arrange
        let expectedStr =
            "CREATE TABLE IF NOT EXISTS category ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = CategoryTable.getCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS category; CREATE TABLE IF NOT EXISTS category ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = CategoryTable.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
