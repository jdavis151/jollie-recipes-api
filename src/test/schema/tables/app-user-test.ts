import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import AppUserTable from "../../../main/schema/tables/app-user";
import { DataType } from "../../../main/schema/utils/data-types";

describe("App User Table class", function () {
    let idCol: Column = new Column("id", DataType.Serial, true);
    let nameCol: Column = new Column("name", DataType.String, false, true);
    let rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    it("static properties", function () {
        // Arrange/Act/Assert
        expect(AppUserTable.getTableName()).to.equal("appUser");
        expect(AppUserTable.getIdColumn()).to.deep.equal(idCol);
        expect(AppUserTable.getColumnMap()).to.deep.equal([
            idCol,
            nameCol,
            rectimestampCol,
        ]);
    });

    it("getDropString", function () {
        // Arrange
        let expectedStr = "DROP TABLE IF EXISTS appUser;";

        // Act
        let actualStr = AppUserTable.getDropString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCreationString", function () {
        // Arrange
        let expectedStr =
            "CREATE TABLE IF NOT EXISTS appUser ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = AppUserTable.getCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS appUser; CREATE TABLE IF NOT EXISTS appUser ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = AppUserTable.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
