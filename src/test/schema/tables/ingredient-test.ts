import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import IngredientTable from "../../../main/schema/tables/ingredient";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Ingredient Table class", function () {
    let idCol: Column = new Column("id", DataType.Serial, true);
    let nameCol: Column = new Column(
        "name",
        DataType.String,
        false,
        true,
        true
    );
    let descCol: Column = new Column(
        "description",
        DataType.String,
        false,
        true
    );
    let rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    it("static properties", function () {
        // Arrange/Act/Assert
        expect(IngredientTable.getTableName()).to.equal("ingredient");
        expect(IngredientTable.getIdColumn()).to.deep.equal(idCol);
        expect(IngredientTable.getColumnMap()).to.deep.equal([
            idCol,
            nameCol,
            descCol,
            rectimestampCol,
        ]);
    });

    it("getDropString", function () {
        // Arrange
        let expectedStr = "DROP TABLE IF EXISTS ingredient;";

        // Act
        let actualStr = IngredientTable.getDropString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCreationString", function () {
        // Arrange
        let expectedStr =
            "CREATE TABLE IF NOT EXISTS ingredient ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = IngredientTable.getCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS ingredient; CREATE TABLE IF NOT EXISTS ingredient ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = IngredientTable.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
