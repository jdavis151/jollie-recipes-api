import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import InstructionTable from "../../../main/schema/tables/instruction";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Instruction Table class", function () {
    let idCol: Column = new Column("id", DataType.Serial, true);
    let titleCol: Column = new Column("title", DataType.String, false, true);
    let descCol: Column = new Column(
        "description",
        DataType.String,
        false,
        true
    );
    let recipeCol: Column = new Column(
        "recipeId",
        DataType.Serial,
        false,
        true
    );
    let orderCol: Column = new Column("order", DataType.Integer);
    let rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    it("static properties", function () {
        // Arrange/Act/Assert
        expect(InstructionTable.getTableName()).to.equal("instruction");
        expect(InstructionTable.getIdColumn()).to.deep.equal(idCol);
        expect(InstructionTable.getColumnMap()).to.deep.equal([
            idCol,
            titleCol,
            descCol,
            recipeCol,
            orderCol,
            rectimestampCol,
        ]);
    });

    it("getDropString", function () {
        // Arrange
        let expectedStr = "DROP TABLE IF EXISTS instruction;";

        // Act
        let actualStr = InstructionTable.getDropString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCreationString", function () {
        // Arrange
        let expectedStr =
            "CREATE TABLE IF NOT EXISTS instruction ( id SERIAL PRIMARY KEY, title VARCHAR NOT NULL, description VARCHAR NOT NULL, recipeId SERIAL NOT NULL, order INTEGER, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = InstructionTable.getCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS instruction; CREATE TABLE IF NOT EXISTS instruction ( id SERIAL PRIMARY KEY, title VARCHAR NOT NULL, description VARCHAR NOT NULL, recipeId SERIAL NOT NULL, order INTEGER, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = InstructionTable.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
