import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import RecipeTable from "../../../main/schema/tables/recipe";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Recipe Table class", function () {
    let idCol: Column = new Column("id", DataType.Serial, true);
    let nameCol: Column = new Column(
        "name",
        DataType.String,
        false,
        true,
        true
    );
    let descCol: Column = new Column("description", DataType.String);
    let rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    it("let properties", function () {
        // Arrange/Act/Assert
        expect(RecipeTable.getTableName()).to.equal("recipe");
        expect(RecipeTable.getIdColumn()).to.deep.equal(idCol);
        expect(RecipeTable.getColumnMap()).to.deep.equal([
            idCol,
            nameCol,
            descCol,
            rectimestampCol,
        ]);
    });

    it("getDropString", function () {
        // Arrange
        let expectedStr = "DROP TABLE IF EXISTS recipe;";

        // Act
        let actualStr = RecipeTable.getDropString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCreationString", function () {
        // Arrange
        let expectedStr =
            "CREATE TABLE IF NOT EXISTS recipe ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = RecipeTable.getCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS recipe; CREATE TABLE IF NOT EXISTS recipe ( id SERIAL PRIMARY KEY, name VARCHAR NOT NULL UNIQUE, description VARCHAR, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = RecipeTable.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
