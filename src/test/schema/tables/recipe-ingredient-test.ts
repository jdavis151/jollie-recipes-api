import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import RecipeIngredientTable from "../../../main/schema/tables/recipe-ingredient";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Recipe-Ingredient Table class", function () {
    let idCol: Column = new Column("id", DataType.Serial, true);
    let ingredientCol: Column = new Column(
        "ingredient",
        DataType.Serial,
        false,
        true
    );
    let recipeCol: Column = new Column("recipe", DataType.Serial, false, true);
    let quantityCol: Column = new Column(
        "quantity",
        DataType.String,
        false,
        true
    );
    let rectimestampCol: Column = new Column(
        "rectimestamp",
        DataType.Timestamp,
        false,
        true
    );

    it("let properties", function () {
        // Arrange/Act/Assert
        expect(RecipeIngredientTable.getTableName()).to.equal(
            "recipeIngredient"
        );
        expect(RecipeIngredientTable.getIdColumn()).to.deep.equal(idCol);
        expect(RecipeIngredientTable.getColumnMap()).to.deep.equal([
            idCol,
            ingredientCol,
            recipeCol,
            quantityCol,
            rectimestampCol,
        ]);
    });

    it("getDropString", function () {
        // Arrange
        let expectedStr = "DROP TABLE IF EXISTS recipeIngredient;";

        // Act
        let actualStr = RecipeIngredientTable.getDropString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCreationString", function () {
        // Arrange
        let expectedStr =
            "CREATE TABLE IF NOT EXISTS recipeIngredient ( id SERIAL PRIMARY KEY, ingredient SERIAL NOT NULL, recipe SERIAL NOT NULL, quantity VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = RecipeIngredientTable.getCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });

    it("getCleanCreationString", function () {
        // Arrange
        let expectedStr =
            "DROP TABLE IF EXISTS recipeIngredient; CREATE TABLE IF NOT EXISTS recipeIngredient ( id SERIAL PRIMARY KEY, ingredient SERIAL NOT NULL, recipe SERIAL NOT NULL, quantity VARCHAR NOT NULL, rectimestamp TIMESTAMP NOT NULL );";

        // Act
        let actualStr = RecipeIngredientTable.getCleanCreationString();

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
