import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Class Column functions", function () {
    it("ctor - simple", function () {
        // Arrange/Act
        let column: Column = new Column("test", DataType.Boolean);

        // Assert
        expect(column).to.exist;
        expect(column.getName()).to.equal("test");
        expect(column.getType()).to.equal(DataType.Boolean);
    });

    it("getCreationString - simple", function () {
        // Arrange
        let expectedStr = "test VARCHAR";
        let column: Column = new Column("test", DataType.String);

        // Act
        let testStr = column.getCreationString();

        // Assert
        expect(testStr).to.exist;
        expect(testStr).to.equal(expectedStr);
    });

    it("getCreationString - complex", function () {
        // Arrange
        let expectedStr =
            "test VARCHAR NOT NULL UNIQUE PRIMARY KEY CHECK (test)";
        let column: Column = new Column(
            "test",
            DataType.String,
            true,
            true,
            true,
            "test"
        );

        // Act
        let testStr = column.getCreationString();

        // Assert
        expect(testStr).to.exist;
        expect(testStr).to.equal(expectedStr);
    });

    it("getAdditionString - simple", function () {
        // Arrange
        let expectedStr = "ADD COLUMN test VARCHAR";
        let column: Column = new Column("test", DataType.String);

        // Act
        let testStr = column.getAdditionString();

        // Assert
        expect(testStr).to.exist;
        expect(testStr).to.equal(expectedStr);
    });

    it("getAdditionString - complex", function () {
        // Arrange
        let expectedStr =
            "ADD COLUMN test VARCHAR NOT NULL UNIQUE PRIMARY KEY CHECK (test)";
        let column: Column = new Column(
            "test",
            DataType.String,
            true,
            true,
            true,
            "test"
        );

        // Act
        let testStr = column.getAdditionString();

        // Assert
        expect(testStr).to.exist;
        expect(testStr).to.equal(expectedStr);
    });
});
