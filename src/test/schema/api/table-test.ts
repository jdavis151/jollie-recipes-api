import { expect } from "chai";
import Column from "../../../main/schema/api/column";
import Table from "../../../main/schema/api/table";
import { DataType } from "../../../main/schema/utils/data-types";

describe("Abstract Class Table functions", function () {
    let idCol = new Column("id", DataType.String, true);
    let timeCol = new Column("rectimestamp", DataType.Timestamp, false, true);

    class TestTable extends Table {
        static tableName: string = "testTable";
        static idColumn: Column = idCol;
        static columnMap: Column[] = [idCol, timeCol];
    }

    let testTable: TestTable;

    before(function () {
        testTable = new TestTable();
    });

    it("ctor - default", function () {
        // Arrange/Act/Assert
        expect(TestTable).to.exist;
        expect(TestTable.getTableName()).to.equal("testTable");
        expect(TestTable.getIdColumn()).to.deep.equal(idCol);
        expect(TestTable.getColumnMap()).to.deep.equal([idCol, timeCol]);
    });
});
