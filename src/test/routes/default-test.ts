import { expect } from "chai";
import { server, BASE_URL } from "../setup";

describe("Default Route Test", () => {
    it("get base url", (done) => {
        server
            .get(`${BASE_URL}/`)
            .expect(200)
            .end((err: any, res: any) => {
                expect(res.status).to.equal(200);
                expect(res.body.message).to.exist;
                expect(res.body.message).to.equal("Default Route");
                done();
            });
    });
});
