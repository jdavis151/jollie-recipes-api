import { expect } from "chai";
import { server, BASE_URL } from "../setup";

describe("Ingredient Route Test", () => {
    it("get ingredient url", (done) => {
        server
            .get(`${BASE_URL}/ingredient/`)
            .expect(200)
            .end((err: any, res: any) => {
                expect(res.status).to.equal(200);
                expect(res.body.message).to.exist;
                expect(res.body.message).to.equal("Ingredient Route");
                done();
            });
    });
});
