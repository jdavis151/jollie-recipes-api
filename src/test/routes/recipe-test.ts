import { expect } from "chai";
import { server, BASE_URL } from "../setup";

describe("Recipe Route Test", () => {
    it("get recipe url", (done) => {
        server
            .get(`${BASE_URL}/recipe/`)
            .expect(200)
            .end((err: any, res: any) => {
                expect(res.status).to.equal(200);
                expect(res.body.message).to.exist;
                expect(res.body.message).to.equal("Recipe Route");
                done();
            });
    });
});
