import { expect } from "chai";
import { server, BASE_URL } from "../setup";

describe("User Route Test", () => {
    it("get user url", (done) => {
        server
            .get(`${BASE_URL}/user/`)
            .expect(200)
            .end((err: any, res: any) => {
                expect(res.status).to.equal(200);
                expect(res.body.message).to.exist;
                expect(res.body.message).to.equal("User Route");
                done();
            });
    });
});
