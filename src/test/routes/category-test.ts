import { expect } from "chai";
import { server, BASE_URL } from "../setup";

describe("Category Route Test", () => {
    it("get category url", (done) => {
        server
            .get(`${BASE_URL}/category/`)
            .expect(200)
            .end((err: any, res: any) => {
                expect(res.status).to.equal(200);
                expect(res.body.message).to.exist;
                expect(res.body.message).to.equal("Category Route");
                done();
            });
    });
});
