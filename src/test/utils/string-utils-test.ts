import { expect } from "chai";
import { trim } from "../../main/utils/string-utils";

describe("String Utils", function () {
    it("trim", function () {
        // Arrange
        let expectedStr = "this string has lots of spaces";
        let testStr = "  this   string has lots  of spaces            ";

        // Act
        let actualStr = trim(testStr);

        // Assert
        expect(actualStr).to.equal(expectedStr);
    });
});
